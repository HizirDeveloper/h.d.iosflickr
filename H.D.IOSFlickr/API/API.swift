//
//  API.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//
import Foundation

enum API: String {
	case URLFlickr = "https://api.flickr.com/services/rest/"
}
