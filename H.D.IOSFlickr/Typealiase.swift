//
//  Typealiase.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//

import Foundation
import UIKit

typealias Clousure<T>        = ((T) -> ())
typealias ClousureArrayPhoto = ((UIImage?, Int) -> ())
typealias ClousureData       = ((Data) -> ())
