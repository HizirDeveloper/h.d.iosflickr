//
//  CollageViewController.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//
import UIKit

class CollageViewController: UIViewController {
	//MARK: --- VARIBELS
	public var arrayPhotos = Photos()
	public var arrayImage  = UIImage()
	public var popularPhoto: PopularPhoto!
	//MARK: --- OUTLETS
	@IBOutlet var arrayImageView      : [UIImageView]!
	@IBOutlet weak var changeImageView: UIImageView!
	@IBOutlet weak var changeView     : UIView!
	//MARK: --- LOAD
	override func viewDidLoad() {
		super.viewDidLoad()
		setupSelfController()
	}
	//MARK: --- FUNCTIONS
	private func setupSelfController(){
		self.createPopularPhoto()
		self.addChangeView()
		self.arrayImageView.forEach{$0.shadowAndCornerRadius()}
	}
}

