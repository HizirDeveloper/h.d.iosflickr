//
//  CollageFunction.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//
import UIKit
// Functions
extension CollageViewController {
	
	//добовляем вью для отображения выбраного изабражения
	public func addChangeView(){
		setupChangeView()
		animationAddCangeView()
	}
	//добовляем первые четые изображения в UIImages
	public func addImageInChangeView(image: UIImage?){
		self.changeImageView.image = image
		animationAddCangeView()
	}
	//отбираем первые четыре изаброжения
	public func createPopularPhoto(){
		self.popularPhoto = PopularPhoto(arrayPhotos: arrayPhotos, countPhoto: 4)
		self.popularPhoto.createPopularPhoto { [unowned self] (image, index) in
			self.arrayImageView[index].image = image
		}
	}
	//настроики вью(с выбраной картинкой)
	public func setupChangeView(){
		let width : CGFloat = self.view.frame.width - 18
		let height: CGFloat = width
		self.changeView.frame  = CGRect(x: 0, y: 0, width: width, height: height)
		self.changeView.center = self.view.center
		self.view.addSubview(changeView)
		self.changeView.shadowAndCornerRadius()
		self.changeImageView.shadowAndCornerRadius()
	}
	//анимация вью с выбраным изаброжением 
	public func animationAddCangeView() {
		UIView.animate(withDuration: 0.5) { [unowned self] in
			if self.changeView.alpha.isZero {
				self.changeView.transform = .identity
				self.changeView.alpha = 1
			} else {
				self.changeView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
				self.changeView.alpha = 0
				self.changeImageView.transform = .identity
			}
		}
	}
}
