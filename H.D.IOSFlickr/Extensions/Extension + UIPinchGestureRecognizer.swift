//
//  Extension + UIPinchGestureRecognizer.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//

import UIKit

extension UIPinchGestureRecognizer {
	
	func pinchGesture(){
		if self.state == .began || self.state == .changed {
			guard let view  = self.view  else { return }
			view.transform = view.transform.scaledBy(x: self.scale, y: self.scale)
			self.scale = 1.0
		}
	}
}
