//
//  BrandsDetailJSON.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//

import Foundation

struct BrandDetailJSON: Decodable {
	let cameras: Cameras?
}

struct Cameras: Decodable {
	let brand : String?
	let camera: [Camera]?
}

struct Camera: Decodable {
	let id     : String?
	let name   : Name?
	let images : Images?
}

struct Name: Decodable {
	let _content: String
}

struct Images: Decodable {
	let small: Small?
	let large: Large?
}

struct Small: Decodable {
	let _content: String?
}

struct Large: Decodable {
	let _content: String?
}
