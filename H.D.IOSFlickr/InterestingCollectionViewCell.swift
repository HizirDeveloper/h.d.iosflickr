//
//  InterestingCollectionViewCell.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//

import UIKit

class InterestingCollectionViewCell: UICollectionViewCell {
	
	private var loadPhotoManager = LoadPhotoManager()
	
	@IBOutlet private weak var photoImageView   : UIImageView!
	@IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet private weak var paginationLabel  : UILabel!
	
	public func configurCell(index: Int, photo: Photo?){
		guard let photo = photo else { return }
		self.loadPhoto(stringURL: photo.urlPhoto())
		self.paginationLabel.text = "\(index)"
	}
	private func loadPhoto(stringURL: String){
		self.loadPhotoManager.transferData(stringURL: stringURL) { [unowned self] (image) in
			self.photoImageView.image = image as? UIImage
			self.activityIndicator.show(on: false)
		}
	}
	override func awakeFromNib() {
		super.awakeFromNib()
		activityIndicator.show(on: true)
	}
	override func layoutSubviews() {
		super.layoutSubviews()
		self.shadowAndCornerRadius()
	}
}
