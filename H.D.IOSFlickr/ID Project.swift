//
//  ID Project.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//
import Foundation

enum ID: String {
	//Segue
	case SgInteresting_Collage
	case SgBrands_BrandDetail
}
