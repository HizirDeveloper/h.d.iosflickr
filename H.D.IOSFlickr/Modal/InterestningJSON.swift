//
//  InterestningJSON.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//
import Foundation

struct InterestningJSON: Decodable {
	let photos: Photos?
}

import Foundation

struct Photos: Decodable {
	
	var page	 : Int?
	var pages	 : Int?
	var perpage: Int?
	var photo  : [Photo]?
}

struct Photo: Decodable {
	
	let id    : String?
	let owner : String?
	let secret: String?
	let server: String?
	let farm  : Int?
	let title : String?
	//создаем УРЛ для фотографий 
	public func urlPhoto() -> String {
		guard let id = self.id, let farm = self.farm, let server = self.server, let secret = self.secret else { return "" }
		let url = URLPhoto(id: id, farm: farm, server: server, secret: secret)
		return url.urlPhoto
	}
}
struct URLPhoto {
	
	private	let id       : String
	private	let farm_id  : Int
	private	let server_id: String
	private	let secret   : String
	private	let size     : String
	
	public var urlPhoto: String {
		let url = "https://farm" + "\(farm_id)" + ".staticflickr.com/" + server_id + "/" + id + "_" + secret + size + ".jpg"
		return url
	}
	init(id: String, farm: Int, server: String, secret: String, size: String = "_m") {
		self.id        = id
		self.farm_id   = farm
		self.server_id = server
		self.secret    = secret
		self.size      = size
	}
}

