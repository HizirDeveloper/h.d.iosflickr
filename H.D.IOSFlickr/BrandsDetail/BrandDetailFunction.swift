//
//  BrandDetailFunction.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//

extension BrandDetailViewController {
	//загружаем JSON c информацией о выбраном бренде
	public func loadJSONBrandDetail(barand: String){
		let methodValue = URLParameters.Value.MDListModels.rawValue
		let stringURL   = API.URLFlickr.rawValue
		let parameters  = URLParameters.create(brand: barand, methodValue: methodValue)
		self.loadJSONManager.transferData(stringURL: stringURL, parameters: parameters){ [unowned self] (json) in
			guard	let camera = (json as! BrandDetailJSON).cameras?.camera else { return }
			self.arrayCamera = camera
			self.activityIndicator.show(on: false)
			self.tableView.reloadData()
		}
	}
}

