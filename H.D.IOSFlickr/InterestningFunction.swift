//
//  InterestningFunction.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//
import Foundation

extension InterestningViewController {
	
	//загружаем JSON c интересными картинками
	public func loadJSONAndReloadCollection(){
		let method     = URLParameters.Value.MDInterestosti.rawValue
		let stringURL  = API.URLFlickr.rawValue
		let parameters = URLParameters.create(methodValue: method)
		loadJSONManager.transferData(stringURL: stringURL, parameters: parameters) { [unowned self] (json) in
			guard let photos = (json as? InterestningJSON)?.photos else { return }
			self.arrayPhotos = photos
			self.collectionView.reloadData()
			self.activityIndicator.show(on: false)
		}
	}
}
