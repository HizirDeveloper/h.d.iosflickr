//
//  BrandsJSON.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//

struct BrandsJSON: Decodable {
	let brands: Brands?
}

struct Brands: Decodable {
	let brand: [Brand]?
}

struct Brand: Decodable {
	
	let id  : String
	let name: String
}
