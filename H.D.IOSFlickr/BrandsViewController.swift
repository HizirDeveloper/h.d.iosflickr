//
//  BrandsViewController.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//

import UIKit

class BrandsViewController: UIViewController {
	//MARK: --- VARIBELS
	public let loadJSONManager    = LoadJSONManager<BrandsJSON>()
	public var searchBrandsCamers = [Brand]()
	public var brandsCamers       : [Brand]?
	public var searchController   : UISearchController!
	//MARK: --- OUTLETS
	@IBOutlet public weak var tableView        : UITableView!
	@IBOutlet public weak var activityIndicator: UIActivityIndicatorView!
	//MARK: --- LOAD
	override func viewDidLoad() {
		super.viewDidLoad()
		setupSelfController()
	}
	//MARK: --- FUNCTIONS
	private func setupSelfController(){
		parseJSONAndTableViewReload()
		activityIndicator.show(on: true)
		setupSearchController()
	}
	//MARK: --- SEGUES
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segue.identifier {
		case ID.SgBrands_BrandDetail.rawValue:
			guard let brandName = (sender as? Brand)?.name else { return }
			(segue.destination as! BrandDetailViewController).brandName = brandName
		default:
			break
		}
	}
}
