//
//  BrandsTableViewCell.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//

import UIKit

class BrandsTableViewCell: UITableViewCell {
	
	public var brand: Brand? {
		didSet{
			guard let brand = brand else { return  }
			self.textLabel?.text = brand.name
		}
	}
}
