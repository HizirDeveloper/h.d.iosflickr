//
//  Alerts.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//
import UIKit

struct Alerts {
	//алерт предупреждение если нет интернета
	static func notNetwork(vc: UIViewController){
		let alert = UIAlertController(title: StringProject.alertNotNetwork.rawValue, message: nil, preferredStyle: .alert)
		vc.present(alert, animated: true)
	}
}
