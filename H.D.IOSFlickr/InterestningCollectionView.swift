//
//  InterestningCollectionView.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//
import UIKit

//MARK: --- DataSorce
extension InterestningViewController: UICollectionViewDataSource {
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		guard let arrayPhotos = arrayPhotos.photo else { return 0 }
		return arrayPhotos.count
	}
}
//MARK: --- Delegate
extension InterestningViewController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: InterestingCollectionViewCell.identifire, for: indexPath) as! InterestingCollectionViewCell
		cell.configurCell(index: (indexPath.row + 1), photo: arrayPhotos.photo?[indexPath.row])
		return cell
	}
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
		
	}
}
//MARK: --- Autolayaut
extension InterestningViewController:  UICollectionViewDelegateFlowLayout {
	//Размер ячеек
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
		let width  = (collectionView.frame.width / 2) - 20
		let height = width
		return CGSize(width: width, height: height)
	}
}
