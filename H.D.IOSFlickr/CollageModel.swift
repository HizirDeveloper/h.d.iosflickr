//
//  CollageModel.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//
import UIKit

struct PopularPhoto {
	
	private var arrayPhotos      : Photos!
	private let loadPhotoManager = LoadPhotoManager()
	private let countPhoto       : Int!
	
	public func createPopularPhoto(_ complition: @escaping ClousureArrayPhoto){
		guard let photo = arrayPhotos.photo else { return  }
		for (index, image) in photo.enumerated() {
			guard index < countPhoto else { return  }
			loadPhotoManager.transferData(stringURL: image.urlPhoto()) { (image) in
				complition(image as? UIImage, index)
			}
		}
	}
	init(arrayPhotos: Photos, countPhoto: Int) {
		self.arrayPhotos = arrayPhotos
		self.countPhoto  = countPhoto
	}
}
