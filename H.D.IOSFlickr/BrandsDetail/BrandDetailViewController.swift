//
//  BrandDetailViewController.swift
//  H.D.IOSFlickr
//
//  Created by -=HIZIR=- on 10/03/2019.
//  Copyright © 2019 GurobaDeveloper. All rights reserved.
//

import UIKit

class BrandDetailViewController: UIViewController {
//MARK: --- VARIBELS
	public var arrayCamera     : [Camera]!
	public var brandName       = String()
	public let loadJSONManager = LoadJSONManager<BrandDetailJSON>()
//MARK: --- OUTLETS
	@IBOutlet weak var tableView        : UITableView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
//MARK: --- LOAD
	override func viewDidLoad() {
		super.viewDidLoad()
		setupSelfController()
	}
//MARK: --- FUNCTIONS
	private func setupSelfController(){
		self.activityIndicator.show(on: true)
		self.loadJSONBrandDetail(barand: brandName)
	}
}
